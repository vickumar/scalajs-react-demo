package example

import com.xored.scalajs.react._
import com.xored.scalajs.react.util._
import org.scalajs.dom._

import scala.scalajs.js.annotation.JSExport

import scala.util.Random

object CarForm extends TypedReactSpec with TypedEventListeners {
  case class Props(onSubmit: (CarModel) => Unit)

  case class State(car: CarModel)

  def getUniqueId() = {
    val newId = Random.nextInt() % 1000
    if (newId < 0)
      newId * -1
    else newId
  }

  def getInitialState(self: This) =
    State(CarModel(getUniqueId(), "", "", 2017, "", 0))

  implicit class Closure(self: This) {
    import self._

    def handleChange = input.onChange(e => {
      e.target.name match {
        case "newMake" => self.setState(state.copy(self.state.car.copy(make = e.target.value)))
        case "newModel" => self.setState(state.copy(self.state.car.copy(model = e.target.value)))
        case "newYear" => self.setState(state.copy(self.state.car.copy(year = e.target.value.toInt)))
        case "newColor" => self.setState(state.copy(self.state.car.copy(color = e.target.value)))
        case "newPrice" => self.setState(state.copy(self.state.car.copy(price = e.target.value.toDouble)))
      }
    })

    def handleSave = input.onClick(e => {
      self.props.onSubmit(self.state.car)
      setState(State(CarModel(getUniqueId(), "", "", 2017, "", 0)))
    })
  }

  @scalax
  def render(self: This) = {
    <form>
      <div className="form-group">
        <label htmlFor="new-make-input">Make</label>
        <input type="text" id="new-make-input" name="newMake"
               value={self.state.car.make}
               onChange={self.handleChange}
               className="form-control" />
      </div>
      <div className="form-group">
        <label htmlFor="new-model-input">Model</label>
        <input type="text" id="new-model-input" name="newModel"
               value={self.state.car.model}
               onChange={self.handleChange}
               className="form-control" />
      </div>
      <div className="form-group">
        <label htmlFor="new-year-input">Year</label>
        <input type="text" id="new-year-input" name="newYear"
               value={self.state.car.year.toString}
               onChange={self.handleChange}
               className="form-control" />
      </div>
      <div className="form-group">
        <label htmlFor="new-color-input">Color</label>
        <input type="text" id="new-color-input" name="newColor"
               value={self.state.car.color}
               onChange={self.handleChange}
               className="form-control" />
      </div>
      <div className="form-group">
        <label htmlFor="new-price-input">Price</label>
        <input type="text" id="new-price-input" name="newPrice"
               value={self.state.car.price.toString}
               onChange={self.handleChange}
               className="form-control" />
      </div>

      <div className="form-group">
        <input type="button" value="Add New"
               onClick={self.handleSave}
               className="btn btn-success"/>
      </div>
    </form>
  }
}
