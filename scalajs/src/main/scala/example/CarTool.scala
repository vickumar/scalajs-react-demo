package example

import com.xored.scalajs.react._

object CarTool extends TypedReactSpec {
  case class Props(cars: List[CarModel])
  case class State(cars: List[CarModel])

  def getInitialState(self: This) = State(self.props.cars)

  implicit class Closure(self: This) {
    import self._

    def addCar(newCar: CarModel) = {
      setState(state.copy(cars = state.cars ++ List(newCar)))
    }

    def deleteCar(carId: Int) = {
      setState(state.copy(cars = state.cars.filterNot(c => c.id == carId)))
    }

    def saveCar(carUpdate: CarModel) = {
      val oldCars = state.cars.filterNot(c => c.id == carUpdate.id)
      setState(state.copy(cars = oldCars ++ List(carUpdate)))
    }
  }

  @scalax
  def render(self: This) = {
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <h1>Cars List</h1>
        </div>
      </div>
      <div className="row">
        <div className="col-md-9">
          {
            CarTable(CarTable.Props(
              cars=self.state.cars,
              onDelete=self.deleteCar,
              onSave=self.saveCar))
          }
        </div>
        <div className="col-md-3">
          {
            CarForm(CarForm.Props(onSubmit=self.addCar))
          }
        </div>
      </div>
    </div>
  }
}
