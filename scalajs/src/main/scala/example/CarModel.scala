package example

case class CarModel(id: Int, make: String, model: String, year: Integer, color: String, price: Double)