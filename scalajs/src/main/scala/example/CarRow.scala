package example

import com.xored.scalajs.react.util.TypedEventListeners
import com.xored.scalajs.react._

object CarRow extends TypedReactSpec with TypedEventListeners {
  case class Props(car: CarModel,
                   onDelete: (Int) => Unit,
                   onSave: (CarModel) => Unit)

  case class State(car: CarModel, editMode: Boolean = false)

  def getInitialState(self: This) = State(self.props.car)

  implicit class Closure(self: This) {
    import self._

    def handleChange = input.onChange(e => {
      e.target.name match {
        case "editMake" => self.setState(state.copy(self.state.car.copy(make = e.target.value)))
        case "editModel" => self.setState(state.copy(self.state.car.copy(model = e.target.value)))
        case "editYear" => self.setState(state.copy(self.state.car.copy(year = e.target.value.toInt)))
        case "editColor" => self.setState(state.copy(self.state.car.copy(color = e.target.value)))
        case "editPrice" => self.setState(state.copy(self.state.car.copy(price = e.target.value.toDouble)))
      }
    })

    def handleSave = button.onClick(e => {
      self.props.onSave(self.state.car.copy())
      setState(State(self.state.car.copy(), false))
    })

    def handleEdit = button.onClick(e => {
        setState(State(self.state.car.copy(), true))
      })

    def handleDelete = button.onClick(e => {
        self.props.onDelete(self.props.car.id)
      })

    def handleCancel = button.onClick(e => {
      setState(State(self.state.car.copy(), false))
    })
  }

  @scalax
  def render(self: This) = {
    if (self.state.editMode) {
      <tr>
        <td><input type="text" id="edit-make-input" name="editMake"
                   value={self.state.car.make} className="form-control"
                   onChange={self.handleChange} />
        </td>
        <td><input type="text" id="edit-model-input" name="editModel"
                   value={self.state.car.model} className="form-control"
                   onChange={self.handleChange} />
        </td>
        <td><input type="text" id="edit-year-input" name="editYear"
                   value={self.state.car.year.toString} className="form-control"
                   onChange={self.handleChange} />
        </td>
        <td><input type="text" id="edit-color-input" name="editColor"
                   value={self.state.car.color} className="form-control"
                   onChange={self.handleChange} />
        </td>
        <td><input type="text" id="edit-price-input" name="editPrice"
                   value={self.state.car.price.toString} className="form-control"
                   onChange={self.handleChange} />
        </td>
        <td>
          <button className="btn-primary" onClick={self.handleSave}>Save</button> |
          <button className="btn-warning" onClick={self.handleCancel}>Cancel</button>
        </td>
      </tr>
    }
    else {
      <tr>
        <td>
          {self.props.car.make}
        </td>
        <td>
          {self.props.car.model}
        </td>
        <td>
          {self.props.car.year}
        </td>
        <td>
          {self.props.car.color}
        </td>
        <td>
          {self.props.car.price}
        </td>
        <td>
          <button className="btn-primary" onClick={self.handleEdit}>Edit</button>
          |
          <button className="btn-danger" onClick={self.handleDelete}>Delete</button>
        </td>
      </tr>
    }
  }
}