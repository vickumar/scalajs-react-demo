package example

import com.xored.scalajs.react.{scalax, React}

import scala.scalajs.js
import org.scalajs.dom

object ScalaJSExample extends js.JSApp {
  def main(): Unit = {
    val cars = List(
      CarModel(1, "Toyota", "Camry", 2015, "red", 20000.0),
      CarModel(2, "Honda", "Camry", 2015, "blue", 20000.0)
    )

    React.renderComponent(
      CarTool(CarTool.Props(cars)),
      dom.document.body
    )
  }
}
