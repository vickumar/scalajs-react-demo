package example

import com.xored.scalajs.react._

object CarTable extends TypedReactSpec {
  case class Props(cars: List[CarModel],
                   onDelete: (Int) => Unit,
                   onSave: (CarModel) => Unit)
  case class State()

  def getInitialState(self: This) = State()

  @scalax
  def render(self: This) = {
    <table className="table table-responsive table-striped">
      <thead>
        <tr>
          <th>Make</th>
          <th>Model</th>
          <th>Year</th>
          <th>Color</th>
          <th colspan="2">Price</th>
        </tr>
      </thead>
      <tbody>
        {
          self.props.cars.map(c => {
            CarRow(CarRow.Props(
              car=c,
              onDelete=self.props.onDelete,
              onSave=self.props.onSave))
          })
        }
      </tbody>
    </table>
  }
}